# Nomad Demo http-echo app

Basic demo showing how to deploy an app to a Nomad cluster using a "job" .hcl file

This repo is public, do not include any senstive information!

## References

- https://gitlab.com/cgbaker/demo-webapp/-/tree/master/
- https://www.youtube.com/watch?v=jpTFZNFHz1o
- https://stackoverflow.com/questions/63601913/nomad-and-port-mapping
- https://medium.com/hashicorp-engineering/hashicorp-nomad-from-zero-to-wow-1615345aa539#444a
- https://github.com/hashicorp/levant/blob/main/docs/commands.md