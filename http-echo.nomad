job "webapp-job" {
  datacenters = ["PartnerOne"]
  type = "service"

  group "webapp-group" {
    count = 3

    network {
      # dynamic ports
      port "http" {}
      # mapped ports
      #port "http" { to = 80 }
      # static ports
      #port "http" { static = 80 }
    }

    task "webapp-task" {
      driver = "docker"
      config {
        image = "hashicorp/http-echo:latest"
        args  = [
          "-listen", ":${NOMAD_PORT_http}",
          "-text", "Hello and welcome to ${NOMAD_IP_http} running on port ${NOMAD_PORT_http}",
        ]
        ports = ["http"]
      }

      service {
        name = "webapp-service"
        port = "http"
        tags = [
          "webapp",
        ]
        check {
          type     = "http"
          path     = "/health"
          interval = "2s"
          timeout  = "2s"
        }
      }
    }
  }
}
